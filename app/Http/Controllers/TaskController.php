<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        //
        $tasks = Task::latest()->paginate(4);
        return view('index', ['tasks' => $tasks]);
        // return view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(): View
    {
        //
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): RedirectResponse
    {
        //

        $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);
        // dd($request->all());
        Task::create($request->all());
        return redirect()->route('tasks.index')->with('success','nueva tarea creada exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task): View
    {
        //
        
        return view('edit', ['task' =>$task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task): RedirectResponse
    {
        //
        // dd($request->all());
        $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);
        $task -> update($request->all());
        return redirect()->route('tasks.index')->with('success', 'nueva tarea editada exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task): RedirectResponse
    {
        //
        // dd($task);
        $task->delete();
        return redirect()->route('tasks.index')->with('success', 'tarea eliminada exitosamente');
    }
}
